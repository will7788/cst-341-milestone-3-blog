package com.spring.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.servlet.ModelAndView;

@Controller
public class BlogController {
	//Main method calling helper functions. Mapped to http://localhost:8080/Spring-MVC-LoginFor/blog
	
	   @RequestMapping("/blog")  
	   public ModelAndView blog(HttpServletRequest request,
			   HttpServletResponse response) {
		 //gets session, verifies the session is not null and gets the username for when a new blog is entered. 
		   HttpSession session = request.getSession();
		   
		      String userName= "";
			if(session.getAttribute("username")!= null) {
		    	  userName = session.getAttribute("username").toString();
		      }
		   //acquire title and entry data from new blog form
			  String title=request.getParameter("title");  
		      String entry=request.getParameter("entry");

		      //calls get_connection helper method
		      try {
				get_connection(userName, title, entry);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				System.out.println("unsuccessful in adding the post!");
				e.printStackTrace();
			}
		   //display updated page
		   return new ModelAndView("added", 
		    		  "message", userName + " we added your entry!"); 

}
	   
	   
	   //Method to make a connection to database
		public Connection get_connection(String username, String title, String Entry) throws SQLException {
			Connection connection = null;

		
				try {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/blog", "root", "");
					System.out.println("Connection SUccessful!!!");
				} catch ( ClassNotFoundException e) {
					System.out.println("Connection Fail!!!");
					System.out.print(e);
				}
				//calls helper method
				newEntry(username, title, Entry);
				

			return connection;
			
		}   
		
		//method to add a new extry to the DB. takes the current username from session, and the title and entry information
		//and inputs into database.
		public void newEntry(String username, String title, String entry) throws SQLException {
			Connection connection = null;

		
				try {
					Class.forName("com.mysql.jdbc.Driver");
					connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/blog", "root", "");
					System.out.println("Connection SUccessful!!!");
				} catch ( ClassNotFoundException e) {
					System.out.println("Connection Fail!!!");
					System.out.print(e);
				}
				
				
				//this code may be used to increment later on for sorting methods.
				/*Statement stmtID = connection.createStatement();
				ResultSet getID =stmtID.executeQuery("SELECT * FROM USERS");
				int ID = 0;
				while(getID.next()) {
					ID++;
				}
				
				ID= ID +1;*/
				
				
				Statement stmnt = connection.createStatement();
				String rs = "INSERT INTO `blogs` (`username`, `title`, `content`) VALUES('" + username + "','" +  title + "','" + entry +  "')";
				
				stmnt.execute(rs);
				
			
		}
	   
	   
}