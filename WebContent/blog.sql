-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 20, 2019 at 05:13 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `username` varchar(100) NOT NULL,
  `title` varchar(50) NOT NULL,
  `content` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`username`, `title`, `content`) VALUES
('Csciarrino', 'Entry 1', 'A new Blog\r\n         '),
('Csciarrino', 'My second Entry', 'Entry number 2. This has been a very challenging endeavor!!\r\n         '),
('Csciarrino', 'Third Entry', 'New entry! yayy\r\n         '),
('Mike', 'Hello my name is Mike', 'This is my first blog post!\r\n         '),
('Csciarrino', 'My fourth blog', 'I am so shocked this works...\r\n         '),
('Csciarrino', 'And the final test', 'Thank goodness...  '),
('Csciarrino', 'Entry 5 ', 'trying something else\r\n         '),
('Csciarrino', 'One more time', 'WORK PLZZZ\r\n         '),
('BobRoss', 'Let Us Paint', 'A happy little tree in the woods\r\n         ');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `password` varchar(50) NOT NULL,
  `ID` int(11) NOT NULL,
  `username` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`firstName`, `lastName`, `age`, `password`, `ID`, `username`) VALUES
('Michael', 'Sciarrino', 30, 'Mike', 1, 'Mike'),
('Crystal', 'Sciarrino', 26, 'Sciarrino', 2, 'Csciarrino'),
('Bob', 'Ross', 100, 'happyTree', 3, 'BobRoss');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
